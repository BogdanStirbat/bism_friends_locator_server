<?php
include_once('AndroidApi.php');

if(isset($_POST['data'])) {
    $api = new AndroidApi();
    
    try{
        if($api->setParams($_POST['data'])->insert()) {
            echo $api::reply(1, $api->getResponse());
        }
    } catch(Exception $e) {
        echo $api::reply(0, $e->getMessage());
    }
} else {
    echo AndroidApi::reply(0, "No 'data' key in POST received");
}