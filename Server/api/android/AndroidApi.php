<?php

include_once('../Api.php');
include_once('../../utils/Model/AndroidModel.php');
include_once('../../utils/Model/SessionModel.php');

class AndroidApi extends Api {
    
    const ONE_PHONE_ONE_SESSION_RESTRICTION = true;
    
    public function __construct() {
        parent::__construct();
        $model = AndroidModel::getSingleton();
        $this->setModel($model);
    }
    
    public function __destruct() {
        $this->getModel()->closeConnection();
        parent::__destruct();
    }
    
    public function insert() {
        $params = $this->getParams();
        if($params->name && $params->phone) {
            $response = $this->getModel()->createAndroid($params->name, $params->phone);
            if($response) {
                $this->setResponse($response);
                return true;
            }
        } else {
            throw new Exception("Mandatory: Parameters 'name' and 'phone' not received");
        }
        throw new Exception("Error creating session: ".$this->getModel()->getError());
    }

    public function insertTracking() {
      $params = $this->getParams();
      if($params->latitude && $params->longitude) {
        
        if(!$this->getPhoneId() || !$this->getSessionId()) {
            throw new Exception("Error creating tracking: PhoneId or SessionId could not be found for the received params: ".$params->toString());
        }
        
        $response = $this->getModel()->createTracking($params->latitude, $params->longitude, $this->getPhoneId(), $this->getSessionId());
        if($response) {
          $this->setResponse($response);
          return true;
        }
      } else {
          throw new Exception("Mandatory: 'latitude' and 'longitude' not received");
      }
      throw new Exception("Error creating mapping: ".$this->getModel()->getError());
    }

    public function updateMap() {
      $params = $this->getParams();
      if($params->session_id) {
        $phoneId = $this->getPhoneId();
        if (!$phoneId) {
          throw new Exception("Mandatory: 'phone' or 'phone_id' is mandatory");
        }
        $response = $this->getModel()->getTrackingData($params->session_id, $phoneId);
        if($this->getModel()->getError()) {
          throw new Exception("Error getting tracking: ".$this->getModel()->getError());
        }

        $this->setResponse($response);
        return true;
      } else {
        throw new Exception("Mandatory: 'session_id' is mandatory");
      }

      throw new Exception("An error has occuered");
    }
    
    public function get() {
    
      $phoneId = $this->getPhoneId();
      if($phoneId) {
        $response = $this->getModel()->getAndroid($phoneId);
        if($this->getModel()->getError()) {
          throw new Exception("Error getting android data: ".$this->getModel()->getError());
        }
        $this->setResponse($response);
        return true;
      }

      throw new Exception("Mandatory: Phone id or phone is mandatory in the request json");
    }
    
    private function getPhoneId() {
        $phoneId = $this->getParams()->phone_id;
        if(!$phoneId && $this->getParams()->phone) {
            $phoneId = $this->getModel()->getIdByPhoneNumber($this->getParams()->phone);
            $this->getParams()->phone_id = $phoneId;
        }
        return $phoneId;
    }
    
    private function getSessionId() {
        $sessionId = $this->getParams()->session_id;
        if(!$sessionId && self::ONE_PHONE_ONE_SESSION_RESTRICTION) {
            $sessionId = $this->getSessionModel()->getSessionIdByAndroidId($this->getPhoneId());
            $this->getParams()->session_id = $sessionId;
        }
        
        return $sessionId;
    }
    
    
    private function getSessionModel() {
        return SessionModel::getSingleton();
    }
}