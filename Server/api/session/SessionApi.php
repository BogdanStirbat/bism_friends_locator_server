<?php

include_once('../Api.php');
include_once('../../utils/Model/SessionModel.php');
include_once('../../utils/Model/AndroidModel.php');

class SessionApi extends Api {
    
    public function __construct() {
        parent::__construct();
        $model = SessionModel::getSingleton();
        $this->setModel($model);
    }
    
    public function __destruct() {
        $this->getModel()->closeConnection();
        parent::__destruct();
    }
    
    public function insert() {
        $params = $this->getParams();
        if($params->name) {
            $response = $this->getModel()->createSession($params->name, $params->description);
            if($response) {
                $this->setResponse($response);
                return true;
            }
        } else {
            throw new Exception("Mandatory: Parameter 'name' not received");
        }
        throw new Exception("Error creating session: ".$this->getModel()->getError());
    }

    public function insertMapping() {
      $params = $this->getParams();
      if($params->session_id) {
        
        $response = $this->getModel()->createMapping($this->getPhoneId(), $params->session_id);
        if($response) {
          $this->setResponse($response);
          return true;
        }
      } else {
          throw new Exception("Mandatory: Parameter 'session_id' not received");
      }
      throw new Exception("Error creating mapping: ".$this->getModel()->getError());
    }
    
    public function checkSession() {
        $phoneId = $this->getPhoneId();
        if(!$phoneId) {
            throw new Exception('Mandatory: Paramater phone or phone_id not given');
        }
        
        $response = $this->getSessionIdByAndroidId($phoneId);
        if($this->getModel()->getError()) {
            throw new Exception("Error getting session ID: ".$this->getModel()->getError());    
        }
        
        $this->setResponse((int) $response);
        return true;
    }
    
    public function disableSession() {
        if(!$this->getParams()->session_id) {
            throw new Exception("Mandatory: Parameter session_id not given");
        }     
        $response = $this->getModel()->disableSession($this->getParams()->session_id);
        if($response) {
            $this->setResponse('');
            return true;
        }
        throw new Exception("Error disabling session:".$this->getParams()->session_id." Error:".$this->getModel()->getError());
    }
    
    public function getSessionIdByAndroidId($phoneId) {
        return $this->getModel()->getSessionIdByAndroidId($phoneId);
    }
    
    
    
    private function getAndroidModel() {
        return AndroidModel::getSingleton();
    }
    
    private function getPhoneId() {
        $phoneId = $this->getParams()->phone_id;
        if(!$phoneId && $this->getParams()->phone) {
            $phoneId = $this->getAndroidModel()->getIdByPhoneNumber($this->getParams()->phone);
            $this->getParams()->phone_id = $phoneId;
        }
        
        return $phoneId;
    }
}