<?php
include_once('SessionApi.php');

if(isset($_POST['data'])) {
  $api = new SessionApi();

  try{
    if($api->setParams($_POST['data'])->insertMapping()) {
      echo $api::reply(1, $api->getResponse());
    }
  } catch(Exception $e) {
    echo $api::reply(0, $e->getMessage());
  }
} else {
  echo SessionApi::reply(0, "No 'data' key in POST received");
}