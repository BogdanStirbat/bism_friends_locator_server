<?php

include_once(dirname(__FILE__).'/../utils/Params.php');

abstract class Api {
    
    private $_model = null;
    private $_params;
    private $_response = '';
    
    public function __construct() {
    }
    
    public function __destruct() {
        $this->clearParams();
    }
    
    protected function setModel($model) {
        $this->_model = $model;
        return $this;
    }
    
    protected function getModel() {
        return $this->_model;
    }
    
    public function setParams($params) {
        $array = json_decode($params, true);
        if (NULL === $array) {
            throw new Exception("Decoding of ".$params." failed\n");
        }
        
        $this->_params = new Params($array);
        
        return $this;
    }
    
    public static function reply($success = 1, $message = null) {
        return json_encode(array('success' => $success, "message" => $message));
    }
    
    protected function clearParams() {
        $this->_params = null;
        $this->_response = '';
        return $this;
    }
    
    protected function getParams() {
        return $this->_params;
    }
    
    public function setResponse($response) {
        $this->_response = $response;
        return $this;
    }
    
    public function getResponse() {
        return $this->_response;
    }
    
    public abstract function insert();
    
    
    
}