<?php

class Params {
    
    private $_array = null;
    
    public function __construct($array = array()) {
        $this->_array = $array;
    }
    
    
    public function __get($name) {
        if(isset($this->_array[$name])) {
            return $this->_array[$name];
        }
        return null;
    }
    
    public function __set($name, $value) {
        $this->_array[$name] = $value;
        return $this;
    }
    
    public function setArray($array) {
        $this->_array = $array;
        return $this;
    }
    
    public function toString() {
        return json_encode($this->_array);
    }
}