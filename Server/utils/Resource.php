<?php

abstract class Resource {
    
    private $_servername = '';
    private $_username = '';
    private $_password = '';
    private $_dbName = '';
    private $_port = '';
    private $_conn;
    
    public function __construct($servername, $username, $password, $database, $port) {
        $this->_servername = $servername;
        $this->_username = $username;
        $this->_password = $password;
        $this->_dbName = $database;
        $this->_port = $port;
        if(!$this->init()) {
            throw new Exception("Couldn't connect to DB: ".$this->_conn->connect_error);
        };
    }
    
    private function init() {
        // Create connection
        $this->_conn = new mysqli($this->_servername, $this->_username, $this->_password, $this->_dbName, $this->_port);

        // Check connection
        if ($this->_conn->connect_error) {
            return false;
        } 
        return true;
    }
    /**
     * Method that runs the given query against the mysql connection
     * @param string $query
     */
    public abstract function runQuery($query);
    
    public abstract function insertData($sql);
    public abstract function updateData($sql);
    public abstract function deleteData($sql);
    public abstract function selectData($sql);
    
    public function closeConnection() {
        $this->_conn->close();
    }
    
    protected function getConnection() {
        return $this->_conn;
    }
    
    public function getError() {
        return $this->_conn->error;
    }
}