<?php
include_once('Resource.php');
include_once('IModel.php');

class Model extends Resource implements IModel {
    const DB_NAME = 'c9';
    const USERNAME = 'alexbanica';
    const PASSWORD = '';
    const SERVERNAME = '127.0.0.1';
    const PORT = 3306;

    public static $instance = null;

    public function runQuery($query) {
        //echo ($query."\n");
        return $this->getConnection()->query($query);
    }
    
    public static function getSingleton() {
        if (!self::$instance) {
            self::$instance = new Model((self::SERVERNAME)? self::SERVERNAME:getenv("REMOTE_ADDR") , self::USERNAME, self::PASSWORD, self::DB_NAME, self::PORT);
        }
        
        return self::$instance;
    }
    
    public function insertData($sql) {
        if (true === $this->runQuery($sql)) {
            return $this->getConnection()->insert_id;
        }
        
        return false;
    }
    
    public function selectData($sql) {
        $result = $this->runQuery($sql);
        
        if (!$result || $result->num_rows <= 0) {
            return array();
        }
        
        $returnValues = array();
        while($row = $result->fetch_assoc()) {
            $returnValues[] = $row;
        }
        
        return $returnValues;
    }
    
    public function updateData($sql) {
        if (true === $this->runQuery($sql)) {
            return true;
        }
        
        return false;
    }
    public function deleteData($sql) {
        ;
    }
}