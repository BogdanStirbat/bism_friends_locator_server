<?php

/**
 * This file will install the mysql tables
 */
 
include_once("../Model.php");

$model = Model::getSingleton();
echo ("> Initialising queries\n");

$queries = array(
    'DROP TABLE IF EXISTS `tracking`',
    'DROP TABLE IF EXISTS `session_mappings`',
    'DROP TABLE IF EXISTS `android_entity`',
    'DROP TABLE IF EXISTS `session_entity`',
    'CREATE TABLE android_entity (
        entity_id INT NOT NULL AUTO_INCREMENT,
        phone_number varchar(11) NOT NULL,
        name varchar(255) NOT NULL,
        PRIMARY KEY (entity_id)
    )',
    'CREATE TABLE session_entity (
        entity_id INT NOT NULL AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        description varchar(1000),
        active INT(1),
        PRIMARY KEY (entity_id)
    )',
    'CREATE TABLE tracking (
        entity_id INT NOT NULL AUTO_INCREMENT,
        android_id INT NOT NULL,
        session_id INT NOT NULL,
        latitude DECIMAL(10,4),
        longitude DECIMAL(10,4),
        created_at TIMESTAMP DEFAULT NOW(),
        PRIMARY KEY (entity_id),
        FOREIGN KEY (android_id)
        REFERENCES android_entity(entity_id),
        FOREIGN KEY (session_id)
        REFERENCES session_entity(entity_id)
    )',
    'CREATE TABLE session_mappings (
        entity_id INT NOT NULL AUTO_INCREMENT,
        android_id INT NOT NULL,
        session_id INT NOT NULL,
        PRIMARY KEY (entity_id),
        FOREIGN KEY (android_id)
        REFERENCES android_entity(entity_id),
        FOREIGN KEY (session_id)
        REFERENCES session_entity(entity_id)
    )'
);

echo ("> Running Queries\n");
foreach($queries as $query) {
    if($model->runQuery($query) === true) {
        echo("> Query successfully ran\n");
    } else {
        echo ('> Query error: '.$model->getError()."\n");
        break;
    }
}

$model->closeConnection();
echo ("> Queries ran, closed connection\n");