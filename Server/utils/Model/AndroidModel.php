<?php

include_once(dirname(__FILE__)."/../Model.php");

class AndroidModel extends Model {
    const TABLE_TRACKING = 'tracking';
    
    const ID = 'entity_id';

   public static $instance = null;
    
    public function createAndroid($phoneNumber, $name) {
        $sql = "INSERT INTO ".self::TABLE_ANDROID.' (phone_number, name) VALUES ("'.$phoneNumber.'","'.$name.'")';
        return $this->insertData($sql);
    }

    public function createTracking($latitude, $longitude, $androidId, $sessionId) {
      $sql = "INSERT INTO ".self::TABLE_TRACKING.' (`session_id`, `android_id`, `latitude`, `longitude`) VALUES ("'.$sessionId.'","'.$androidId.'","'.$latitude.'","'.$longitude.'")';
      return $this->insertData($sql);
    }
    
    public function getIdByPhoneNumber($phoneNumber) {
        $sql = "Select entity_id FROM ".self::TABLE_ANDROID.' WHERE phone_number="'.$phoneNumber.'" LIMIT 1';
        $selectData = $this->selectData($sql);
        if(count($selectData)) {
            if(isset($selectData[0][self::ID])) {
                return $selectData[0][self::ID];
            }
        } else {
            /** create the phone number if it doesn't exist **/
            return $this->createAndroid($phoneNumber,'Anonymous'.time());
        }
        
        return null;
    }
    
    public function getAndroid($phoneId) {
        $sql = 'Select * from "'.self::TABLE_ANDROID.'" where entity_id="'.$phoneId.'"';
        $selectData = $this->selectData($sql);
        if(count($selectData) == 1) {
            return $selectData[0];
        }
        return null;
    }

    public function getTrackingData($sessionId, $phoneId) {
      $sql = 'Select ae.`name`, ae.`phone_number`, t.latitude, t.longitude, t.created_at from '.self::TABLE_TRACKING.' as t LEFT JOIN '.self::TABLE_ANDROID.' as ae on t.android_id = ae.entity_id LEFT JOIN '.self::TABLE_SESSION.' as se on t.`session_id` = se.`entity_id` where se.`active` = 1 and se.`entity_id` = "'.$sessionId.'" ORDER BY t.created_at DESC';
      $selectData = $this->selectData($sql);
      if(count($selectData)) {
        return $selectData;
      }
      return null;
    }


    public static function getSingleton() {
      if (!self::$instance) {
        self::$instance = new AndroidModel((self::SERVERNAME)? self::SERVERNAME:getenv("REMOTE_ADDR") , self::USERNAME, self::PASSWORD, self::DB_NAME, self::PORT);
      }

      return self::$instance;
    }
}