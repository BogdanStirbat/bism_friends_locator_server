<?php

include_once(dirname(__FILE__)."/../Model.php");

class SessionModel extends Model {
    const TABLE_MAPPINGS = 'session_mappings';

    public static $instance = null;
    
    
    public function createSession($sessionName, $sessionDescription = null) {
        $sql = "INSERT INTO `".self::TABLE_SESSION.'` (name, active) VALUES ("'.$sessionName.'","1")';
        if($sessionDescription) {
            $sql = "INSERT INTO ".self::TABLE_SESSION.' (name, description, active) VALUES ("'.$sessionName.'","'.$sessionDescription.'","1")';
        }
        
        return $this->insertData($sql);
    }

    public function createMapping($sessionId, $androidId) {
      $sql = "INSERT INTO ".self::TABLE_MAPPINGS.' (`session_id`, `android_id`) VALUES ("'.$sessionId.'","'.$androidId.'")';
      return $this->insertData($sql);
    }
    
    /**
     * Should be used only if the restriction of ONE_TO_ONE is enabled between android and sessions
     */
    public function getSessionIdByAndroidId($phoneId) {
        $sql = "Select M.session_id FROM ".self::TABLE_MAPPINGS." as M LEFT JOIN ".self::TABLE_SESSION.' as S ON M.session_id = S.entity_id WHERE S.active=1 and M.phone_id="'.$phoneId.'" LIMIT 1';
        $selectData = $this->selectData($sql);
        if(count($selectData)) {
            if(isset($selectData[0]['session_id'])) {
                return $selectData[0]['session_id'];
            }
        }
        return null;
    }
    
    public function disableSession($sessionId) {
        $sql = "UPDATE TABLE ".self::TABLE_SESSION.' SET active=0 WHERE session_id="'.$sessionId.'"';
        return $this->updateData($sql);
    }


    public static function getSingleton() {
      if (!self::$instance) {
        self::$instance = new SessionModel((self::SERVERNAME)? self::SERVERNAME:getenv("REMOTE_ADDR") , self::USERNAME, self::PASSWORD, self::DB_NAME, self::PORT);
      }

      return self::$instance;
    }
}